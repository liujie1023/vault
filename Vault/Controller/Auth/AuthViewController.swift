//
//  AuthViewController.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import LocalAuthentication

class AuthViewController: BaseViewController {
    
    let authContext = LAContext()
    var authError: NSError?
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var imgFingerPrint: UIImageView!
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        initSyncTimer()
    }
    
    func initView() {
        imgLogo.image = AppIcons(strIcon: "splash")?.getIcon()
        imgFingerPrint.image = AppIcons(strIcon: "finger-print")?.getIcon()
    }
    
    func initSyncTimer() {
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(gotoMainView), userInfo: nil, repeats: false)
    }
    
    @objc func gotoMainView() {
        timer?.invalidate()
        timer = nil
        DispatchQueue.main.async {
//            self.appDelegate.makingRootVC("tutorial")
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TutorialVC") as! TutorialViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionAuthWithTouchID(_ sender: UIButton) {
//        if authContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
//            authContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "App needs to verify your Touch ID") { (success, error) in
//                if success {
//                    print("Touch ID Success")
//                    DispatchQueue.main.async {
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TutorialVC") as! TutorialViewController
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
//                }else {
//                    print("Touch ID Failed")
//                }
//            }
//        }else {
//            print("No Touch ID")
//        }
    }
    
    

}
