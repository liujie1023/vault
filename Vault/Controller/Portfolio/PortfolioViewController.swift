//
//  PortfolioViewController.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import Floaty

class PortfolioViewController: BaseViewController {

    @IBOutlet weak var btnFloatMenu: Floaty!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var walletView: WalletsView!
    @IBOutlet weak var vaultChart: VaultChart!
    @IBOutlet weak var btnLeftMenu: UIButton!
    
    var chooseWallet: ChooseWallet?
    var vault_qrscan: VaultQRScan?
    var walletBar: WalletBar?
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        if UIDevice.modelName == "iPhone X" || UIDevice.modelName == "Simulator iPhone X" {
            topView.transform = CGAffineTransform(translationX: 0, y: 380 - appDelegate.topViewHeight!)
            topView.frame = CGRect(x: topView.frame.origin.x, y: topView.frame.origin.y, width: topView.frame.width, height: appDelegate.topViewHeight!)
        }
        
        btnLeftMenu.setImage(AppIcons(strIcon: "left-menu")?.getIcon(), for: .normal)
        
        // Choose Wallet
        chooseWallet = ChooseWallet(frame: self.view.frame)
        chooseWallet?.alpha = 0.0
        chooseWallet?.loadNibName()
        self.view.addSubview(chooseWallet!)
        
        // Vault QR Scan
        vault_qrscan = VaultQRScan(frame: self.view.frame)
        vault_qrscan?.alpha = 0.0
        vault_qrscan?.viewCon = self
        vault_qrscan?.loadNibName()
        self.view.addSubview(vault_qrscan!)
        
        // Init Float Menu
        let send_item = FloatyItem()
        send_item.buttonColor = AppTheme.dark_yellow
        send_item.title = "Send"
        send_item.icon = AppIcons(strIcon: "message-send")?.getIcon()
        let parent = self
        send_item.handler = { (self) in
            // Send handler
            DispatchQueue.main.async {
                
                parent.chooseWallet?.fadeIn(completion: { (success) in
                    
                })
            }
        }
        
        btnFloatMenu.addItem("Receive", icon: AppIcons(strIcon: "db")?.getIcon()) { (item) in
            // Receive handler
            DispatchQueue.main.async {
                
                parent.vault_qrscan?.fadeIn(completion: { (success) in
                    
                })
            }
        }
        btnFloatMenu.addItem(item: send_item)
        btnFloatMenu.sticky = true
        
        //Init WalletView
        walletView.viewCon = self
        walletView.contentInset = UIEdgeInsetsMake(216, 0, 0, 0)
        
        //ChartView init
        vaultChart.loadNibName()
        
        walletBar = WalletBar(frame: self.view.frame)
        self.view.addSubview(walletBar!)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    @IBAction func actionEditMenu(_ sender: UIButton) {
        self.walletBar?.openView()
    }
    
}
