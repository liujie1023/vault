//
//  WalletDetailViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/15/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class WalletDetailViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgWallet: UIImageView!
    @IBOutlet weak var btnDotMenu: UIButton!
    @IBOutlet weak var walletView: WalletsView!
    @IBOutlet weak var vaultChart: VaultChart!
    @IBOutlet weak var topView: UIView!
    var walletBar: WalletBar?
    
    var chooseWallet: ChooseWallet?
    var vault_qrscan: VaultQRScan?
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        if UIDevice.modelName == "iPhone X" || UIDevice.modelName == "Simulator iPhone X" {
            topView.transform = CGAffineTransform(translationX: 0, y: 380 - appDelegate.topViewHeight!)
            topView.frame = CGRect(x: topView.frame.origin.x, y: topView.frame.origin.y, width: topView.frame.width, height: appDelegate.topViewHeight!)
        }
        
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        imgWallet.image = AppIcons(strIcon: "import-wallet")?.getIcon()
        btnDotMenu.setImage(AppIcons(strIcon: "dot")?.getIcon(), for: .normal)
        
        // Choose Wallet
        chooseWallet = ChooseWallet(frame: self.view.frame)
        chooseWallet?.alpha = 0.0
        chooseWallet?.loadNibName()
        self.view.addSubview(chooseWallet!)
        
        // Vault QR Scan
        vault_qrscan = VaultQRScan(frame: self.view.frame)
        vault_qrscan?.alpha = 0.0
        vault_qrscan?.viewCon = self
        vault_qrscan?.loadNibName()
        self.view.addSubview(vault_qrscan!)
        
        //Init WalletView
        walletView.viewCon = self
        walletView.contentInset = UIEdgeInsetsMake(216, 0, 0, 0)
        
        //ChartView init
        vaultChart.loadNibName()
        
        walletBar = WalletBar(frame: self.view.frame)
        self.view.addSubview(walletBar!)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionEditWallet(_ sender: UIButton) {
        self.walletBar?.openView()
    }

    @IBAction func actionSend(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "THVC") as! THViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionReceive(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "THVC") as! THViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
}
