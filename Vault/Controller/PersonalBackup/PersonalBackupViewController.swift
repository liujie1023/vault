//
//  PersonalBackupViewController.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class PersonalBackupViewController: BaseViewController {

    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var imgQrcode: UIImageView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDotMenu: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        scrView.contentSize = CGSize(width: self.view.frame.width, height: 667)
    }
    
    func initView() {
        imgQrcode.image = AppIcons(strIcon: "qrcode")?.getIcon()
        btnDotMenu.setImage(AppIcons(strIcon: "dot")?.getIcon(), for: .normal)
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionContinue(_ sender: UIButton) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.makingRootVC("enterApp")
    }
    

}
