//
//  ImportWalletViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/7/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class ImportWalletViewController: BaseViewController {

    @IBOutlet weak var walletType: WalletType!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var txtKeystore: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnQrcode: UIButton!
    @IBOutlet weak var passwordView: UIView!
    
    var showKeyboard: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionImportWallet(_ sender: UIButton) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.makingRootVC("enterApp")
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionQRScan(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRCodeViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        btnQrcode.setImage(AppIcons(strIcon: "small-qrcode")?.getIcon(), for: .normal)
        walletType.walletType.text = "Keystore"
        walletType.delegate = self
        comments.placeholder = "Keystore JSON"
        txtKeystore.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedStringKey.foregroundColor: UIColor.lightGray])
        self.showKeyboard = false
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        gesture.delegate = self
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc func handleTap() {
        self.view.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(){
        if showKeyboard == false {
            showKeyboard = true
        }
    }
    
    @objc func keyboardWillBeHidden(){
        if showKeyboard == true {
            showKeyboard = false
        }
    }
    
}

extension ImportWalletViewController: WalletTypeDelegate {
    func onClicked(_ cell: WalletTypeCell) {
        walletType.walletType.text = cell.walletType.text
        switch walletType.walletType.text {
        case "Keystore":
            comments.placeholder = "Keystore JSON"
            passwordView.alpha = 1.0
        case "Private Key":
            comments.placeholder = "Private Key"
            passwordView.alpha = 0.0
        case "Mnemonic":
            comments.placeholder = "Words seperate by a space (Usually contains 12 words)"
            passwordView.alpha = 0.0
        default:
            break
        }
    }
}

extension ImportWalletViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view is UITableView){
            return true
        }
        if (touch.view is UITableViewCell){
            return false
        }
        if (touch.view?.superview is UITableViewCell){
            return false
        }
        if (touch.view?.superview?.superview is UITableViewCell){
            return false
        }
        if (touch.view is UITableViewHeaderFooterView){
            return false
        }
        if (touch.view is UIButton){
            return false
        }
        return true
    }
}
