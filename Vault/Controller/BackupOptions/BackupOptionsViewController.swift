//
//  BackupOptionsViewController.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import SVGKit

class BackupOptionsViewController: BaseViewController {

    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var pageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        scrView.contentSize = CGSize(width: self.view.frame.width, height: 667)
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        pageView.image = AppIcons(strIcon: "page4")?.getIcon()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionPersonalBackup(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "PersonalBackupVC") as! PersonalBackupViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionNotBackup(_ sender: UIButton) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.makingRootVC("enterApp")
    }
    
}
