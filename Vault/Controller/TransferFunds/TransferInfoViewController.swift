//
//  TransferInfoViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TransferInfoViewController: BaseViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtNotes: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgQrcode: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        imgQrcode.image = AppIcons(strIcon: "small-qrcode")?.getIcon()
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email or Ethereum address", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
        txtNotes.attributedPlaceholder = NSAttributedString(string: "Notes", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray])
    }
    
    @IBAction func actionContinue(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TransferConfirmVC") as! TransferConfirmViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
