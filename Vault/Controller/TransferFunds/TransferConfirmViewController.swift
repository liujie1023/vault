//
//  TransferConfirmViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TransferConfirmViewController: BaseViewController {

    @IBOutlet weak var btnBack: UIButton!
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionConfirm(_ sender: UIButton) {
//        let vc = storyboard?.instantiateViewController(withIdentifier: "ScanQRVC") as! ScanQRCodeViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        
        appDelegate.makingRootVC("enterApp")
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
