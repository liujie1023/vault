//
//  TransferCalcViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TransferCalcViewController: BaseViewController {

    @IBOutlet weak var usFunds: VaultLabel!
    @IBOutlet weak var ethFunds: VaultLabel!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var calculator: VaultCalculator!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var imgSync: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        imgSync.image = AppIcons(strIcon: "sync")?.getIcon()
        usFunds.font_size = 35
        usFunds.ratio = "USD"
        ethFunds.font_size = 25
        ethFunds.ratio = "ETH"
        calculator.viewCon = self
        if calculator.frame.height > 370 {
            calculator.frame = CGRect(x: calculator.frame.origin.x, y: calculator.frame.origin.y + calculator.frame.height - 370, width: calculator.frame.width, height: 370)
        }else {
            calculator.frame = CGRect(x: calculator.frame.origin.x, y: calculator.frame.origin.y, width: calculator.frame.width, height: calculator.frame.height)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        
        if usFunds.text != "0 USD" {
            let vc = storyboard?.instantiateViewController(withIdentifier: "TransferInfoVC") as! TransferInfoViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
