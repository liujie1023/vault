//
//  TutorialViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TutorialViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnSkip: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    var ftue1: FTUE1?
    var ftue2: FTUE2?
    var ciwallet: CreateImportWallet?

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        // Configure Buttons
        btnBack.alpha = 0
        btnSkip.alpha = 1
        btnNext.alpha = 1
        
        // Configure Page Control
        self.pageControl.numberOfPages = 3
        self.pageControl.currentPage = 0
        self.pageControl.tintColor = UIColor.red
        self.pageControl.pageIndicatorTintColor = AppTheme.vault_color_6
        self.pageControl.currentPageIndicatorTintColor = AppTheme.white
        
        // Init Subviews
        ftue1 = FTUE1(frame: self.scrView.frame)
        ftue2 = FTUE2(frame: self.scrView.frame)
        ciwallet = CreateImportWallet(frame: self.scrView.frame)
        ciwallet?.viewCon = self
        let views: [UIView] = [ftue1!, ftue2!, ciwallet!]
        
        // Configure Scroll View
        scrView.delegate = self
        scrView.isPagingEnabled = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        var frame: CGRect = CGRect(x: 0, y: 0, width: 0, height: 0)
        
        for index in 0 ..< 3 {
            frame.origin.x = self.scrView.frame.size.width * CGFloat(index)
            frame.size = self.scrView.frame.size
            views[index].frame = frame
            self.scrView.addSubview(views[index])
        }
        scrView.contentSize = CGSize(width: self.scrView.frame.size.width * 3, height: self.scrView.frame.size.height)
        pageControl.addTarget(self, action: #selector(changePage(sender:)), for: UIControlEvents.valueChanged)
    }
    
    @objc func changePage(sender: AnyObject) -> () {
        let x = CGFloat(pageControl.currentPage) * scrView.frame.size.width
        scrView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
        switch pageControl.currentPage {
        case 0:
            btnBack.alpha = 0
            btnSkip.alpha = 1
            btnNext.alpha = 1
        case 1:
            btnBack.alpha = 1
            btnSkip.alpha = 1
            btnNext.alpha = 1
        case 2:
            btnBack.alpha = 1
            btnSkip.alpha = 0
            btnNext.alpha = 0
        default:
            break
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
        switch pageControl.currentPage {
        case 0:
            btnBack.alpha = 0
            btnSkip.alpha = 1
            btnNext.alpha = 1
        case 1:
            btnBack.alpha = 1
            btnSkip.alpha = 1
            btnNext.alpha = 1
        case 2:
            btnBack.alpha = 1
            btnSkip.alpha = 0
            btnNext.alpha = 0
        default:
            break
        }
    }
    
    @IBAction func actionPaging(_ sender: UIButton) {
        switch sender.tag {
        case 100:
            pageControl.currentPage = 2
        case 101:
            pageControl.currentPage -= 1
        case 102:
            pageControl.currentPage += 1
        default:
            break
        }
        changePage(sender: UIButton())
    }
    
}
