//
//  TransactionHistoryViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: BaseViewController {

    @IBOutlet weak var marketView: UIScrollView!
    @IBOutlet weak var transactionHistory: VaultTransaction!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var vaultChart: VaultChart!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDotMenu: UIButton!
    
    var vault_qrscan: VaultQRScan?
    var prevButton: Int = 100
    var walletBar: WalletBar?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionTransaction(_ sender: UIButton) {
        buttons[prevButton - 100].removeBottomLine()
        sender.drawBottomLine()
        prevButton = sender.tag
        if sender.tag == 100 {
            marketView.alpha = 0.0
            transactionHistory.alpha = 1.0
        }else {
            marketView.alpha = 1.0
            transactionHistory.alpha = 0.0
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCustomize(_ sender: UIButton) {
        self.walletBar?.openView()
    }
    
    @IBAction func actionSend(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TransferCalcVC") as! TransferCalcViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionReceive(_ sender: UIButton) {
        // Vault QR Scan
        vault_qrscan = VaultQRScan(frame: self.view.frame)
        vault_qrscan?.alpha = 0.0
        vault_qrscan?.viewCon = self
        vault_qrscan?.loadNibName()
        self.view.addSubview(vault_qrscan!)
        vault_qrscan?.fadeIn(completion: { (success) in
            
        })
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        btnDotMenu.setImage(AppIcons(strIcon: "dot")?.getIcon(), for: .normal)
        marketView.contentSize = CGSize(width: self.view.frame.width, height: 572)
        actionTransaction(buttons[prevButton - 100])
        
        walletBar = WalletBar(frame: CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: 150))
        walletBar?.loadNibName()
        self.view.addSubview(walletBar!)
        
        //ChartView init
        vaultChart.loadNibName()
    }

}
