//
//  THViewController.swift
//  Vault
//
//  Created by Liu Jie on 5/10/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class THViewController: UIViewController {

    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnDot: UIButton!
    @IBOutlet weak var thTable: UITableView!
    
    var vault_qrscan: VaultQRScan?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        thTable.separatorStyle = .none
        thTable.allowsSelection = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initView() {
        btnBack.setImage(AppIcons(strIcon: "back")?.getIcon(), for: .normal)
        btnDot.setImage(AppIcons(strIcon: "dot")?.getIcon(), for: .normal)
    }

    @IBAction func actionSend(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TransactionHVC") as! TransactionHistoryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionReceive(_ sender: UIButton) {
//        // Vault QR Scan
//        vault_qrscan = VaultQRScan(frame: self.view.frame)
//        vault_qrscan?.alpha = 0.0
//        vault_qrscan?.viewCon = self
//        vault_qrscan?.loadNibName()
//        self.view.addSubview(vault_qrscan!)
//        vault_qrscan?.fadeIn(completion: { (success) in
//            
//        })
    }
}

extension THViewController: UITableViewDelegate, UITableViewDataSource, THCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "thCell", for: indexPath) as! THCell
        if indexPath.row % 2 == 0 {
            cell.imgSymbol.image = AppIcons(strIcon: "message-send")?.getIcon()
            cell.lblTitle.text = "Sent"
            cell.lblNum1.textColor = AppTheme.vault_color_4
            cell.lblNum2.textColor = AppTheme.vault_color_4
        }else {
            cell.imgSymbol.image = AppIcons(strIcon: "db")?.getIcon()
            cell.lblTitle.text = "Received"
            cell.lblNum1.textColor = AppTheme.vault_color_9
            cell.lblNum2.textColor = AppTheme.vault_color_9
        }
//        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func gotoSocial(_ cell: THCell) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "TransactionHVC") as! TransactionHistoryViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

protocol THCellDelegate {
    func gotoSocial(_ cell: THCell)
}

class THCell: UITableViewCell {
    
    var delegate: THCellDelegate?
    
    @IBOutlet weak var imgSymbol: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNum1: UILabel!
    @IBOutlet weak var lblNum2: UILabel!
    
    @IBAction func actionSocial(_ sender: UIButton) {
        delegate?.gotoSocial(self)
    }
}
