//
//  NickelItem.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class WalletItem {
    var image: UIImage?
    var name: String?
    var tokens: Int?
    var percentage: Double?
    var wallet_amount1: Double?
    var wallet_amount2: Double?
    var type: String?
}
