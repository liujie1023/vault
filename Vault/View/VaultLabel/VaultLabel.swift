//
//  VaultLabel.swift
//  Vault
//
//  Created by Liu Jie on 5/2/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class VaultLabel: UILabel {
    
    var font_size: Int = 25
    var ratio: String?
    
    var inputedNumber: Int? {
        didSet {
            if inputedNumber == 11 {
                if funds.count == 1 {
                    self.funds = "0"
                }else {
                    let start = funds.startIndex
                    let end = funds.index(funds.endIndex, offsetBy: -1)
                    self.funds = String(funds[start ..< end])
                }
            }else {
                if inputedNumber == 9 {
                    if "\(funds).".isNumeric {
                        self.funds = "\(funds)."
                    }
                }else {
                    var number = inputedNumber! + 1
                    if number == 11 {
                        number = 0
                    }
                    if funds == "0" {
                        self.funds = "\(number)"
                    }else {
                        if "\(funds)\(number)".isNumeric {
                            self.funds = "\(funds)\(number)"
                        }
                    }
                    
                }
                
            }
        }
    }
    
    var funds: String = "0" {
        didSet {
            let amountText = NSMutableAttributedString.init(string: "\(funds) \(ratio!)")
        
            // set the custom font and color for the 0,1 range in string
            amountText.setAttributes([NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: CGFloat(font_size))], range: NSMakeRange(0, funds.count))
        
            // set the attributed string to the UILabel object
            self.attributedText = amountText
        }
    }
    


}
