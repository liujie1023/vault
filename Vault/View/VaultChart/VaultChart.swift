//
//  VaultChart.swift
//  Vault
//
//  Created by Liu Jie on 4/29/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit
import Charts

class VaultChart: UIView {

    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet var buttons: [UIButton]!
    
    var prevBtn: Int = 100
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("VaultChart", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.backgroundColor = self.backgroundColor
        self.addSubview(view)
        initView()
        onClick(buttons[prevBtn - 100])
    }

    func initView() {
        chartView.delegate = self
        chartView.chartDescription?.enabled = false
        chartView.dragEnabled = false
        chartView.setScaleEnabled(false)
        chartView.pinchZoomEnabled = false
        chartView.setViewPortOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        let ll1 = ChartLimitLine(limit: 150, label: "")
        ll1.lineWidth = 4
        let leftAxis = chartView.leftAxis
        leftAxis.removeAllLimitLines()
//        leftAxis.addLimitLine(ll1)
        leftAxis.axisMinimum = 0
        leftAxis.axisMaximum = 150
        leftAxis.drawLimitLinesBehindDataEnabled = false
        leftAxis.drawGridLinesEnabled = false
        leftAxis.gridColor = UIColor.clear
        
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLimitLinesBehindDataEnabled = false
        chartView.drawGridBackgroundEnabled = false
        chartView.drawMarkers = false
        
        chartView.legend.enabled = false
        
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.enabled = false
        chartView.leftAxis.enabled = false
        chartView.rightAxis.enabled = false
        
        setDataCount(10, range: UInt32(130))
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        let values = (0..<count).map { (i) -> ChartDataEntry in
            let val = Double(arc4random_uniform(range) + 5)
            return ChartDataEntry(x: Double(i), y: val)
        }
        
        let set1 = LineChartDataSet(values: values, label: nil)
        set1.drawIconsEnabled = false
        
        set1.setColor(AppTheme.themeColor)
        set1.setCircleColor(.white)
        set1.lineWidth = 4
        set1.circleRadius = 6
        set1.drawCircleHoleEnabled = false
        set1.valueFont = .systemFont(ofSize: 9)
        set1.formLineWidth = 2
        set1.formSize = 15
        set1.drawValuesEnabled = false
        
        set1.drawCirclesEnabled = false
        
        let gradientColors = [UIColor.clear.cgColor , ChartColorTemplates.colorFromString("#00000000").withAlphaComponent(0.3).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        set1.mode = .cubicBezier
        
        let data = LineChartData(dataSet: set1)
        
        chartView.data = data
    }
    
    @IBAction func onClick(_ sender: UIButton) {
        buttons[prevBtn - 100].backgroundColor = UIColor.clear
        sender.backgroundColor = AppTheme.vault_color_10
        prevBtn = sender.tag
    }
}

extension VaultChart: ChartViewDelegate {
    
}
