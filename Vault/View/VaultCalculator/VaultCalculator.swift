//
//  VaultCalculator.swift
//  Vault
//
//  Created by Liu Jie on 5/6/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class VaultCalculator: UIView {
    
    var viewCon: TransferCalcViewController?
    
    let percentages: [String] = ["25%", "50%", "75%", "Send max"]
    let numbers: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "0", "backspace"]

    override func layoutSubviews() {
        // Configure Percent Buttons
        var offsetX: CGFloat = 0
        var offsetY: CGFloat = 0
        for i in 0 ..< 4 {
            let button: UIButton = UIButton(frame: CGRect(x: offsetX, y: 7, width: self.frame.width / 4, height: 30))
            offsetX += self.frame.width / 4
            button.setTitle(percentages[i], for: .normal)
            button.setTitleColor(AppTheme.vault_color_1, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
            button.tag = 200 + i
            button.addTarget(self, action: #selector(onPercentage(sender:)), for: UIControlEvents.touchUpInside)
            self.addSubview(button)
        }
        
        // Add Seperator
        let seperator: UIView = UIView(frame: CGRect(x: 0, y: 45, width: self.frame.width, height: 1))
        seperator.backgroundColor = AppTheme.vault_color_8
        self.addSubview(seperator)
        
        offsetX = 0
        offsetY = 45
        
        // Add Number Buttons
        for i in 0 ..< 12 {
            
            offsetX = CGFloat(i % 3) * self.frame.width / 3
            if i > 0 && i % 3 == 0 {
                offsetY += (self.frame.height - 45) / 4
            }
            let button: UIButton = UIButton(frame: CGRect(x: offsetX, y: offsetY, width: self.frame.width / 3, height: (self.frame.height - 45) / 4))
            
            if i == 11 {
                button.setImage(AppIcons(strIcon: "backspace")?.getIcon(), for: .normal)
            }else {
                button.setTitle("\(numbers[i])", for: .normal)
                button.setTitleColor(UIColor.white, for: .normal)
            }
            button.titleLabel?.font = UIFont.systemFont(ofSize: 40)
            button.tag = 100 + i
            button.addTarget(self, action: #selector(onNumber(sender:)), for: UIControlEvents.touchUpInside)
            self.addSubview(button)
        }
    }
    
    @objc func onPercentage(sender: UIButton) {
        
    }
    
    @objc func onNumber(sender: UIButton) {
        viewCon?.usFunds.inputedNumber = sender.tag - 100
        viewCon?.ethFunds.inputedNumber = sender.tag - 100
        
        if viewCon?.usFunds.text != "0 USD" {
            viewCon?.btnNext.backgroundColor = AppTheme.vault_color_7
        }else {
            viewCon?.btnNext.backgroundColor = AppTheme.vault_color_8
        }
    }

}
