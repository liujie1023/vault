//
//  VaultColorPane.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

protocol VaultColorPaneDelegate {
    func actionClicked(_ view: VaultColorPane)
}

class VaultColorPane: UIView {
    
    var delegate: VaultColorPaneDelegate?
    var contentView: UIView?
    var isSelected: Bool? {
        didSet {
            if isSelected! {
                self.contentView?.alpha = 1.0
            }else {
                self.contentView?.alpha = 0.0
            }
        }
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = 3
        self.clipsToBounds = true
        
        contentView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        contentView?.backgroundColor = UIColor.white
        contentView?.layer.cornerRadius = 3
        contentView?.clipsToBounds = true
        
        let subView: UIView = UIView(frame: CGRect(x: 2, y: 2, width: self.frame.width - 4, height: self.frame.height - 4))
        subView.backgroundColor = self.backgroundColor
        subView.layer.cornerRadius = 3
        subView.clipsToBounds = true
        subView.layer.borderColor = AppTheme.vault_color_3.cgColor
        subView.layer.borderWidth = 1
        
        let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        button.addTarget(self, action: #selector(actionClicked), for: UIControlEvents.touchUpInside)
        
        contentView?.alpha = 0.0
        contentView?.addSubview(subView)
        self.addSubview(contentView!)
        self.addSubview(button)
    }
    
    @objc func actionClicked() {
        delegate?.actionClicked(self)
    }

}
