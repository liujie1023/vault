//
//  CreateImportWallet.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class CreateImportWallet: UIView {
    
    var viewCon: TutorialViewController?
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var imgCreateWallet: UIImageView!
    @IBOutlet weak var imgImportWallet: UIImageView!
    @IBOutlet weak var pageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibName()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNibName()
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("CreateImportWallet", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        scrView.contentSize = CGSize(width: self.frame.width, height: 667)
        imgCreateWallet.image = AppIcons(strIcon: "create-wallet")?.getIcon()
        imgImportWallet.image = AppIcons(strIcon: "import-wallet")?.getIcon()
        pageView.image = AppIcons(strIcon: "page3")?.getIcon()
    }

    @IBAction func actionCreateWallet(_ sender: UIButton) {
        let vc = viewCon?.storyboard?.instantiateViewController(withIdentifier: "BackupOptionsVC") as! BackupOptionsViewController
        viewCon?.navigationController?.pushViewController(vc, animated: true)
//        viewCon?.present(vc, animated: true, completion: nil)
    }
    @IBAction func actionImportWallet(_ sender: UIButton) {
        let vc = viewCon?.storyboard?.instantiateViewController(withIdentifier: "ImportWalletVC") as! ImportWalletViewController
        viewCon?.navigationController?.pushViewController(vc, animated: true)
//        viewCon?.present(vc, animated: true, completion: nil)
    }
}
