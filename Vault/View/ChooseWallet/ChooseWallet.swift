//
//  ChooseWallet.swift
//  Vault
//
//  Created by Liu Jie on 4/29/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class ChooseWallet: UIView {

    @IBOutlet weak var walletDrop: VaultDropDownList!
    @IBOutlet weak var currencyDrop: VaultDropDownList!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("ChooseWallet", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.backgroundColor = self.backgroundColor
        self.addSubview(view)
        walletDrop.type = 0
        walletDrop.delegate = self
        currencyDrop.type = 1
        currencyDrop.delegate = self
    }
    @IBAction func actionClose(_ sender: UIButton) {
        self.fadeOut(completion: { (success) in
            
        })
    }
    
    @IBAction func actionContinue(_ sender: UIButton) {
        self.fadeOut(completion: { (success) in
            DispatchQueue.main.async {
                let vc = self.viewController()?.storyboard?.instantiateViewController(withIdentifier: "TransactionHVC") as! TransactionHistoryViewController
                self.viewController()?.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
}

extension ChooseWallet: VaultDropDownDelegate {
    func dismissOtherDrop(_ dropDown: VaultDropDownList) {
        if dropDown == walletDrop && currencyDrop.isOpen && !walletDrop.isOpen{
            currencyDrop.actionOpen(UIButton())
        }else if dropDown == currencyDrop && walletDrop.isOpen && !currencyDrop.isOpen {
            walletDrop.actionOpen(UIButton())
        }
    }
}
