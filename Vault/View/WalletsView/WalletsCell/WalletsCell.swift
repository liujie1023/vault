//
//  WalletsCell.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

protocol WalletsCellDelegate {
    func walletClicked(_ cell : WalletsCell)
}

class WalletsCell: UITableViewCell {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imgDetail: UIImageView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellValue: UILabel!
    
    var delegate: WalletsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgHeader.image = AppIcons(strIcon: "import-wallet")?.getIcon()
        imgDetail.image = AppIcons(strIcon: "omg")?.getIcon()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionWalletClicked(_ sender: UIButton) {
        delegate?.walletClicked(self)
    }
}
