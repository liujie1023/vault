//
//  WalletsView.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

protocol WalletsViewDelegate {
    func walletClicked(_ cell: WalletsCell)
}

class WalletsView: UITableView {
    
    var viewCon: UIViewController?
    var wallets: [WalletItem] = []
    
    let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        self.layer.cornerRadius = 5
        self.showsVerticalScrollIndicator = false
        self.allowsSelection = false
        self.separatorStyle = .none
        self.clipsToBounds = true
        self.backgroundColor = UIColor.clear
        self.register(UINib.init(nibName: "WalletsCell", bundle: nil), forCellReuseIdentifier: "walletCell")
        self.register(UINib.init(nibName: "DetailCell", bundle: nil), forCellReuseIdentifier: "detailCell")
    }

}

extension WalletsView: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewCon is PortfolioViewController {
            let cell = self.dequeueReusableCell(withIdentifier: "walletCell", for: indexPath) as! WalletsCell
            
            switch indexPath.row {
                case 0:
                    cell.headerView.alpha = 1.0
                    cell.cellView.alpha = 0.0
                    cell.headerView.topRound()
                case 1:
                    cell.headerView.alpha = 0.0
                    cell.cellView.alpha = 1.0
                    cell.imgDetail.image = #imageLiteral(resourceName: "eth.png") // AppIcons(strIcon: "eth")?.getIcon()
                    cell.cellName.text = "ETH"
                    cell.cellValue.text = "$2334.234"
                case 2:
                    cell.cellName.text = "EOS"
                    cell.cellValue.text = "$1237.45"
                    cell.headerView.alpha = 0.0
                    cell.cellView.alpha = 1.0
                    cell.imgDetail.image = #imageLiteral(resourceName: "eos.png") // AppIcons(strIcon: "eos")?.getIcon()
                case 3:
                    cell.cellName.text = "VEN"
                    cell.cellValue.text = "$7986.778"
                    cell.headerView.alpha = 0.0
                    cell.cellView.alpha = 1.0
                    cell.imgDetail.image = #imageLiteral(resourceName: "ven.png") // AppIcons(strIcon: "ven")?.getIcon()
                default:
                    cell.cellName.text = "BAT"
                    cell.cellValue.text = "$12383.456"
                    cell.headerView.alpha = 0.0
                    cell.cellView.alpha = 1.0
                    cell.imgDetail.image = #imageLiteral(resourceName: "bat.png") // AppIcons(strIcon: "bat")?.getIcon()
            }
            if indexPath.row == 7 {
                cell.cellView.bottomRound()
                cell.separatorView.alpha = 0.0
            }else {
                cell.separatorView.alpha = 1.0
            }
            cell.delegate = self
            return cell
        }else if viewCon is WalletDetailViewController{
            let cell = self.dequeueReusableCell(withIdentifier: "walletCell", for: indexPath) as! WalletsCell
            
            switch indexPath.row {
            case 0:
                cell.headerView.alpha = 0.0
                cell.cellView.alpha = 1.0
                cell.imgDetail.image = AppIcons(strIcon: "omg")?.getIcon()
                cell.headerView.topRound()
            case 1:
                cell.cellName.text = "ETH"
                cell.cellValue.text = "$1237.45"
                cell.headerView.alpha = 0.0
                cell.cellView.alpha = 1.0
                cell.imgDetail.image = #imageLiteral(resourceName: "eth.png") // AppIcons(strIcon: "eth")?.getIcon()
            case 2:
                cell.cellName.text = "EOS"
                cell.cellValue.text = "$5673.546"
                cell.headerView.alpha = 0.0
                cell.cellView.alpha = 1.0
                cell.imgDetail.image = #imageLiteral(resourceName: "eos.png") // AppIcons(strIcon: "eos")?.getIcon()
            case 3:
                cell.cellName.text = "VEN"
                cell.cellValue.text = "$3464.5"
                cell.headerView.alpha = 0.0
                cell.cellView.alpha = 1.0
                cell.imgDetail.image = #imageLiteral(resourceName: "ven.png") // AppIcons(strIcon: "ven")?.getIcon()
            default:
                cell.cellName.text = "BAT"
                cell.cellValue.text = "$9784.5565"
                cell.headerView.alpha = 0.0
                cell.cellView.alpha = 1.0
                cell.imgDetail.image = #imageLiteral(resourceName: "bat.png") // AppIcons(strIcon: "bat")?.getIcon()
            }
            if indexPath.row == 7 {
                cell.cellView.bottomRound()
//                cell.separatorView.alpha = 0.0
            }
            cell.delegate = self
            return cell
        }else {
            let cell = self.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as! DetailCell
            
            switch indexPath.row {
            case 0:
                cell.headerView.alpha = 1.0
                cell.detailView.alpha = 0.0
                cell.headerLeft.topRightRound()
                cell.headerRight.topLeftRound()
            case 1:
                cell.headerView.alpha = 0.0
                cell.detailView.alpha = 1.0
                cell.imgDetail.image = AppIcons(strIcon: "eth")?.getIcon()
            case 2:
                cell.headerView.alpha = 0.0
                cell.detailView.alpha = 1.0
                cell.imgDetail.image = AppIcons(strIcon: "eos")?.getIcon()
            case 3:
                cell.headerView.alpha = 0.0
                cell.detailView.alpha = 1.0
                cell.imgDetail.image = AppIcons(strIcon: "ven")?.getIcon()
            default:
                cell.headerView.alpha = 0.0
                cell.detailView.alpha = 1.0
                cell.imgDetail.image = AppIcons(strIcon: "bat")?.getIcon()
            }
            if indexPath.row == 7 {
                cell.detailView.bottomRound()
            }
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if viewCon is PortfolioViewController {
            let vc = viewCon as! PortfolioViewController
            let offset = vc.walletView.contentOffset.y + 216
            
            // Header View Trasnform
            if offset < 216 && offset > 0{
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - offset)
                vc.vaultChart.alpha = (216 - offset * 10) / 216
            }
            if offset < 0 {
                vc.vaultChart.alpha = 1.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight!)
            }else if offset > 216 {
                vc.vaultChart.alpha = 0.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - 216)
            }
        }else if viewCon is WalletDetailViewController{
            let vc = viewCon as! WalletDetailViewController
            let offset = vc.walletView.contentOffset.y + 216
            
            // Header View Trasnform
            if offset < 216 && offset > 0{
                vc.vaultChart.alpha = (216 - offset * 10) / 216
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - offset)
            }
            if offset < 0 {
                vc.vaultChart.alpha = 1.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight!)
            }else if offset > 216 {
                vc.vaultChart.alpha = 0.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - 216)
            }
        }else {
            let vc = viewCon as! WalletViewController
            let offset = vc.walletView.contentOffset.y + 216
            
            // Header View Trasnform
            if offset < 216 && offset > 0{
                vc.vaultChart.alpha = (216 - offset * 10) / 216
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - offset)
            }
            if offset < 0 {
                vc.vaultChart.alpha = 1.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight!)
            }else if offset > 216 {
                vc.vaultChart.alpha = 0.0
                vc.topView.frame = CGRect(x: vc.topView.layer.frame.origin.x, y: vc.topView.layer.frame.origin.y, width: vc.topView.bounds.width, height: appDelegate.topViewHeight! - 216)
            }
        }
        
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
    }
    
}

extension WalletsView: WalletsCellDelegate {
    func walletClicked(_ cell: WalletsCell) {
        let indexPath = self.indexPath(for: cell)
        if indexPath?.row == 0 {
            if viewCon is PortfolioViewController {
                let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                let renameBtn = UIAlertAction(title: "Rename", style: .default) { (action) in
                    let vc = self.viewCon as! PortfolioViewController
                    vc.walletBar?.openView()
                }
                let cancelBtn = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                    
                }
                alertController.addAction(renameBtn)
                alertController.addAction(cancelBtn)
                viewCon?.navigationController?.present(alertController, animated: true, completion: nil)
            }else {
                return
            }
        }
        if viewCon is PortfolioViewController {
//            let vc = viewCon?.storyboard?.instantiateViewController(withIdentifier: "WalletVC") as! WalletViewController
//            viewCon?.navigationController?.pushViewController(vc, animated: true)
            let vc = viewCon?.storyboard?.instantiateViewController(withIdentifier: "TransactionHVC") as! TransactionHistoryViewController
            viewCon?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

