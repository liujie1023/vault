//
//  DetailCell.swift
//  Vault
//
//  Created by Liu Jie on 5/10/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var headerLeft: UIView!
    @IBOutlet weak var headerRight: UIView!
    @IBOutlet weak var imgHeader: UIImageView!
    @IBOutlet weak var imgDetail: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgHeader.image = AppIcons(strIcon: "import-wallet")?.getIcon()
        imgDetail.image = AppIcons(strIcon: "omg")?.getIcon()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
