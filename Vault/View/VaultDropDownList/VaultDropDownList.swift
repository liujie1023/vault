//
//  VaultDropDownList.swift
//  Vault
//
//  Created by Liu Jie on 4/29/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

protocol VaultDropDownDelegate {
    func dismissOtherDrop(_ dropDown: VaultDropDownList)
}

class VaultDropDownList: UIView {

    @IBOutlet weak var dropdownImg: UIImageView!
    @IBOutlet weak var img_wallet: UIImageView!
    @IBOutlet weak var name_wallet: UILabel!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var currencyImg: UIImageView!
    
    var delegate: VaultDropDownDelegate?
    
    var type: Int? {
        didSet {
            if type == 0 {
                currencyView.alpha = 0
                img_wallet.image = AppIcons(strIcon: "import-wallet")?.getIcon()
            }else {
                currencyView.alpha = 1.0
                img_wallet.image = AppIcons(strIcon: "ven")?.getIcon()
                currencyImg.image = AppIcons(strIcon: "ven")?.getIcon()
            }
        }
    }
    var isOpen: Bool = false
    var list_items: [WalletItem] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibName()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNibName()
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("VaultDropDownList", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.layer.cornerRadius = 5.0
        self.layer.cornerRadius = 5.0
        view.backgroundColor = self.backgroundColor
        self.addSubview(view)
        dropdownImg.image = AppIcons(strIcon: "dropdown")?.getIcon()
        img_wallet.image = AppIcons(strIcon: "import-wallet")?.getIcon()
    }
    
    private lazy var listView: UITableView = {
        let list = UITableView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height, width: self.frame.size.width, height: 0))
        list.dataSource = self
        list.delegate = self
        list.backgroundColor = self.backgroundColor
        list.layer.cornerRadius = 5.0
        list.separatorStyle = .none
        list.register(UINib.init(nibName: "DropDownCell", bundle: nil), forCellReuseIdentifier: "dropDownCell")
        self.superview?.addSubview(list)
        
        let border = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height - 2, width: self.frame.size.width, height: 2))
        border.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        self.superview?.addSubview(border)
        
        return list
    }()

    @IBAction func actionOpen(_ sender: UIButton) {
        
        if self.isOpen {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.dropdownImg.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi*2))
                self.listView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height-2, width: self.frame.size.width, height: 0)
            }, completion: { (finished) -> Void in
                if finished{
                    self.dropdownImg.transform = CGAffineTransform(rotationAngle: 0.0)
                    self.isOpen = false
                }
            })
        }else {
            delegate?.dismissOtherDrop(self)
            listView.reloadData()
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.dropdownImg.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                self.listView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height-2, width: self.frame.size.width, height: 150)
            }, completion: { (finished) -> Void in
                if finished{
                    self.isOpen = true
                }
            })
        }
    }
}

extension VaultDropDownList: UITableViewDelegate, UITableViewDataSource {
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dropDownCell", for: indexPath) as! DropDownCell
        cell.backgroundColor = tableView.backgroundColor
        if self.type == 0 {
            cell.imgWallet.image = AppIcons(strIcon: "import-wallet")?.getIcon()
            cell.walletName.text = "Wallet \(indexPath.row + 1)"
        }else {
            
            switch indexPath.row {
            case 0:
                cell.imgWallet.image = #imageLiteral(resourceName: "omg.png") // AppIcons(strIcon: "omg")?.getIcon()
                cell.walletName.text = "OMG"
            case 1:
                cell.imgWallet.image = #imageLiteral(resourceName: "eth.png") // AppIcons(strIcon: "eth")?.getIcon()
                cell.walletName.text = "ETH"
            case 2:
                cell.imgWallet.image = #imageLiteral(resourceName: "eos.png") // AppIcons(strIcon: "eos")?.getIcon()
                cell.walletName.text = "EOS"
            case 3:
                cell.imgWallet.image = #imageLiteral(resourceName: "ven.png") // AppIcons(strIcon: "ven")?.getIcon()
                cell.walletName.text = "VEN"
            default:
                cell.imgWallet.image = #imageLiteral(resourceName: "bat.png") // AppIcons(strIcon: "bat")?.getIcon()
                cell.walletName.text = "BAT"
            }
            
            if self.type == 1 {
                cell.bkView.backgroundColor = UIColor.clear
            }else {
                cell.bkView.backgroundColor = AppTheme.vault_color_1
            }
            
            
        }
        return cell
    }
    
    open func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = AppTheme.vault_color_11
        cell?.backgroundColor = AppTheme.vault_color_11
    }
    
    open func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = AppTheme.vault_color_3
        cell?.backgroundColor = AppTheme.vault_color_3
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actionOpen(UIButton())
//        let vc = self.viewController()?.storyboard?.instantiateViewController(withIdentifier: "TransactionHVC") as! TransactionHistoryViewController
//        self.viewController()?.navigationController?.pushViewController(vc, animated: true)
    }
}
