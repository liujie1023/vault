//
//  DropDownCell.swift
//  Vault
//
//  Created by Liu Jie on 4/30/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class DropDownCell: UITableViewCell {

    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var imgWallet: UIImageView!
    @IBOutlet weak var walletName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
