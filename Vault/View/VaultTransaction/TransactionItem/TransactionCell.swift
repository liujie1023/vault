//
//  TransactionCell.swift
//  Vault
//
//  Created by Liu Jie on 5/4/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet weak var profile: VaultProfile!
    @IBOutlet weak var percentage: UILabel!
    @IBOutlet weak var transaction: UILabel!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
