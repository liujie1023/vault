//
//  VaultTransaction.swift
//  Vault
//
//  Created by Liu Jie on 5/4/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class VaultTransaction: UITableView {

    var viewCon: PortfolioViewController?
    var wallets: [WalletItem] = []
    
    override func awakeFromNib() {
        self.delegate = self
        self.dataSource = self
        self.layer.cornerRadius = 5
        self.showsVerticalScrollIndicator = false
        self.separatorStyle = .none
        self.allowsSelection = false
        self.clipsToBounds = true
        self.backgroundColor = AppTheme.vault_color_3
        self.register(UINib.init(nibName: "TransactionCell", bundle: nil), forCellReuseIdentifier: "transactionCell")
    }

}

extension VaultTransaction: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionCell
        if indexPath.row % 2 == 0 {
            cell.percentage.textColor = AppTheme.vault_color_4
            cell.transaction.textColor = AppTheme.vault_color_4
            cell.profile.status = 0
            cell.lblDateTime.text = "Sent 4/3/2018 03:45"
        }else {
            cell.percentage.textColor = AppTheme.vault_color_5
            cell.transaction.textColor = AppTheme.vault_color_5
            cell.profile.status = 1
            cell.lblDateTime.text = "Received 4/3/2018 03:45"
        }
        
        switch indexPath.row {
        case 0:
            cell.lblname.text = "Thomas P."
            cell.profile.image = AppIcons(strIcon: "sample_avatar")?.getIcon()
            break
        case 1:
            cell.lblname.text = "Coinbase"
            cell.profile.image = AppIcons(strIcon: "unnamed")?.getIcon()
            break
        case 2:
            cell.lblname.text = "1R9337G..."
            cell.profile.image = #imageLiteral(resourceName: "wallet") // AppIcons(strIcon: "import-wallet")?.getIcon()
            break
        case 3:
            cell.lblname.text = "Coinbase"
            cell.profile.image = AppIcons(strIcon: "unnamed")?.getIcon()
            break
        case 4:
            cell.lblname.text = "1R9337G..."
            cell.profile.image = #imageLiteral(resourceName: "wallet") // AppIcons(strIcon: "import-wallet")?.getIcon()
            break
        case 5:
            cell.lblname.text = "Harold"
            cell.profile.image = AppIcons(strIcon: "sample_avatar")?.getIcon()
            break
        default:
            cell.lblname.text = "Harold"
            cell.profile.image = AppIcons(strIcon: "sample_avatar")?.getIcon()
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}
