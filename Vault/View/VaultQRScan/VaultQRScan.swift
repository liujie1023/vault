//
//  VaultQRScan.swift
//  Vault
//
//  Created by Liu Jie on 5/4/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class VaultQRScan: UIView {
    
    var viewCon: UIViewController?
    @IBOutlet weak var imgQrcode: UIImageView!
    @IBOutlet weak var btnNewwindow: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("VaultQRScan", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.backgroundColor = self.backgroundColor
        self.addSubview(view)
        imgQrcode.image = AppIcons(strIcon: "qrcode")?.getIcon()
        btnNext.setImage(AppIcons(strIcon: "next")?.getIcon(), for: .normal)
        btnNewwindow.setImage(AppIcons(strIcon: "newwindow")?.getIcon(), for: .normal)
    }
    
    @IBAction func actionCopy(_ sender: UIButton) {
        
    }
    @IBAction func actionClose(_ sender: UIButton) {
        self.fadeOut { (success) in }
    }
    
    @IBAction func actionNext(_ sender: UIButton) {
        if viewCon is PortfolioViewController {
            let vc = viewCon?.storyboard?.instantiateViewController(withIdentifier: "THVC") as! THViewController
            self.viewCon?.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
