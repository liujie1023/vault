//
//  WalletBar.swift
//  Vault
//
//  Created by Liu Jie on 5/4/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class WalletBar: UIView {
    
    @IBOutlet var styles: [UIButton]!
    @IBOutlet var colors: [VaultColorPane]!
    @IBOutlet weak var txtWalletName: UITextField!
    @IBOutlet weak var editView: UIView!
    
    var viewCon: UIViewController?
    var prevStyle: Int = 100
    var prevColor: Int = 200
    var showKeyboard: Bool?
    
    var keyboardHeight: CGFloat = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibName()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNibName()
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("WalletBar", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        self.showKeyboard = false
        for i in 0 ..< colors.count {
            colors[i].delegate = self
        }
        
        styles[0].setImage(AppIcons(strIcon: "money")?.getIcon(), for: .normal)
        styles[1].setImage(AppIcons(strIcon: "book")?.getIcon(), for: .normal)
        styles[2].setImage(AppIcons(strIcon: "db")?.getIcon(), for: .normal)
        styles[3].setImage(AppIcons(strIcon: "import-wallet")?.getIcon(), for: .normal)
        styles[4].setImage(AppIcons(strIcon: "star")?.getIcon(), for: .normal)
        styles[5].setImage(AppIcons(strIcon: "heart")?.getIcon(), for: .normal)
        styles[6].setImage(AppIcons(strIcon: "flag")?.getIcon(), for: .normal)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        self.addGestureRecognizer(gesture)
        
        addObservers()
        
        self.editView.transform = CGAffineTransform(translationX: 0, y: 150)
        
        self.alpha = 0.0
        
    }

    @IBAction func actionSave(_ sender: UIButton) {
        closeView()
    }
    
    @objc func handleTap() {
//        self.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func openView() {
        onWalletStyle(styles[prevStyle - 100])
        actionClicked(colors[prevColor - 200])
        
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
        
            self.alpha = 1.0
            self.editView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.txtWalletName.becomeFirstResponder()
            
//        }, completion: {(success) in
//
//        })
    }
    
    func closeView() {
        self.endEditing(true)
//        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
        
            self.alpha = 0.0
            self.editView.transform = CGAffineTransform(translationX: 0, y: 150)
            
//        }, completion: {(success) in
//
//        })
    }
    
    @objc func keyboardWasShown(_ notification: Notification){
        
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.keyboardHeight = keyboardRectangle.height
            
            if showKeyboard == false {
                showKeyboard = true
                //            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
                
                self.editView.transform = CGAffineTransform(translationX: 0, y: -self.keyboardHeight)
                
                //            }, completion: {(success) in
                //
                //            })
            }
        }
        
    }
    
    @objc func keyboardWillBeHidden(){
        if showKeyboard == true {
            showKeyboard = false
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseOut, animations: {
            
                self.editView.transform = CGAffineTransform(translationX: 0, y: 0)
                
//            }, completion: {(success) in
//
//            })
        }
    }
    
    @IBAction func onWalletStyle(_ sender: UIButton) {
        styles[prevStyle - 100].layer.borderWidth = 0
        styles[sender.tag - 100].layer.borderWidth = 1
        styles[sender.tag - 100].clipsToBounds = true
        styles[sender.tag - 100].layer.borderColor = UIColor.white.cgColor
        prevStyle = sender.tag
    }
}

extension WalletBar: VaultColorPaneDelegate {
    func actionClicked(_ view: VaultColorPane) {
        colors[prevColor - 200].isSelected = false
        colors[view.tag - 200].isSelected = true
//        colors[0].isSelected = true
        prevColor = view.tag
    }
}

extension WalletBar: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true
    }
}
