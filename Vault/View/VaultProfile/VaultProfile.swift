//
//  VaultProfile.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class VaultProfile: UIImageView {
    
    var img: UIImageView?
    var bkView: UIView?
    var status: Int? {
        didSet {
            if status == 0 {
                img?.image = AppIcons(strIcon: "message-send")?.getIcon()
                bkView?.backgroundColor = AppTheme.dark_yellow
                self.backgroundColor = AppTheme.dark_yellow
            }else {
                img?.image = AppIcons(strIcon: "db")?.getIcon()
                bkView?.backgroundColor = AppTheme.vault_color_2
                self.backgroundColor = AppTheme.vault_color_2
            }
        }
    }

    override func awakeFromNib() {
        self.layer.cornerRadius = self.frame.width / 2
        self.backgroundColor = AppTheme.dark_yellow
        
        img = UIImageView(frame: CGRect(x: 5, y: 5, width: 10, height: 10))
        img?.image = AppIcons(strIcon: "message-send")?.getIcon()
        
        bkView = UIView(frame: CGRect(x: self.frame.origin.x + self.frame.width - 20, y: self.frame.origin.y + self.frame.height - 20, width: 20, height: 20))
        bkView?.layer.cornerRadius = 10
        bkView?.layer.borderWidth = 2
        bkView?.layer.borderColor = AppTheme.vault_color_3.cgColor
        bkView?.clipsToBounds = true
        bkView?.backgroundColor = AppTheme.dark_yellow
        
        bkView?.addSubview(img!)
        self.superview?.addSubview(bkView!)
    }
    

}
