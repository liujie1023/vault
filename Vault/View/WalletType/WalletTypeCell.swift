//
//  WalletTypeCell.swift
//  Vault
//
//  Created by Liu Jie on 5/7/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class WalletTypeCell: UITableViewCell {

    @IBOutlet weak var walletType: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
