//
//  WalletType.swift
//  Vault
//
//  Created by Liu Jie on 5/7/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

protocol WalletTypeDelegate {
    func onClicked(_ cell: WalletTypeCell)
}

class WalletType: UIView {

    @IBOutlet weak var walletType: UILabel!
    @IBOutlet weak var dropdownImg: UIImageView!
    
    var isOpen: Bool = false
    var list_items: [WalletItem] = []
    var delegate: WalletTypeDelegate?
    let types: [String] = ["Keystore", "Private Key", "Mnemonic"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibName()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNibName()
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("WalletType", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.layer.cornerRadius = 5.0
        self.layer.cornerRadius = 5.0
        view.backgroundColor = self.backgroundColor
        self.addSubview(view)
        dropdownImg.image = AppIcons(strIcon: "dropdown")?.getIcon()
    }
    
    private lazy var listView: UITableView = {
        let list = UITableView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height, width: self.frame.size.width, height: 0))
        list.dataSource = self
        list.delegate = self
        list.backgroundColor = self.backgroundColor
        list.layer.cornerRadius = 5.0
        list.separatorStyle = .none
        list.register(UINib.init(nibName: "WalletTypeCell", bundle: nil), forCellReuseIdentifier: "typeCell")
        self.superview?.addSubview(list)
        
        let border = UIView(frame: CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height - 2, width: self.frame.size.width, height: 2))
        border.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
        self.superview?.addSubview(border)
        
        return list
    }()
    
    @IBAction func actionOpen(_ sender: UIButton) {
        
        if self.isOpen {
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.dropdownImg.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi*2))
                self.listView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height-2, width: self.frame.size.width, height: 0)
            }, completion: { (finished) -> Void in
                if finished{
                    self.dropdownImg.transform = CGAffineTransform(rotationAngle: 0.0)
                    self.isOpen = false
                }
            })
        }else {
            listView.reloadData()
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.dropdownImg.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                self.listView.frame = CGRect(x: self.frame.origin.x, y: self.frame.origin.y + self.frame.size.height-2, width: self.frame.size.width, height: 150)
            }, completion: { (finished) -> Void in
                if finished{
                    self.isOpen = true
                }
            })
        }
    }
}

extension WalletType: UITableViewDelegate, UITableViewDataSource {
    open func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typeCell", for: indexPath) as! WalletTypeCell
        cell.backgroundColor = tableView.backgroundColor
        cell.walletType.text = types[indexPath.row]
        return cell
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        actionOpen(UIButton())
        let cell = tableView.cellForRow(at: indexPath)
        delegate?.onClicked(cell as! WalletTypeCell)
    }
}
