//
//  FTUE1.swift
//  Vault
//
//  Created by Liu Jie on 5/5/18.
//  Copyright © 2018. All rights reserved.
//

import UIKit

class FTUE1: UIView {

    @IBOutlet weak var pageView: UIImageView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNibName()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNibName()
    }
    
    func loadNibName() {
        let view = Bundle.main.loadNibNamed("FTUE1", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
        pageView.image = AppIcons(strIcon: "page1")?.getIcon()
    }

}
