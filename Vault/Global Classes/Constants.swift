//
//  Constants.swift
//  Vault
//
//  Created by RSS on 4/30/18.
//  Copyright © 2018 HTK. All rights reserved.
//

import UIKit
import SVGKit

struct AppTheme {
    static let dark_yellow: UIColor = UIColor(red: 250.0 / 255, green: 165.0 / 255, blue: 60.0 / 255, alpha: 1.0)
    static let white: UIColor = UIColor.white
    static let themeColor: UIColor = UIColor(red: 0.0 / 255, green: 220.0 / 255, blue: 190.0 / 255, alpha: 1.0)
    static let vault_color_1: UIColor = UIColor(red: 100.0 / 255, green: 136.0 / 255, blue: 234.0 / 255, alpha: 1.0)
    static let vault_color_2: UIColor = UIColor(red: 0.0 / 255, green: 100.0 / 255, blue: 190.0 / 255, alpha: 1.0)
    static let vault_color_3: UIColor = UIColor(red: 49.0 / 255, green: 58.0 / 255, blue: 86.0 / 255, alpha: 1.0)
    static let vault_color_4: UIColor = UIColor(red: 143.0 / 255, green: 151.0 / 255, blue: 173.0 / 255, alpha: 1.0)
    static let vault_color_5: UIColor = UIColor(red: 0.0 / 255, green: 204.0 / 255, blue: 168.0 / 255, alpha: 1.0)
    static let vault_color_6: UIColor = UIColor(red: 14.0 / 255, green: 40.0 / 255, blue: 52.0 / 255, alpha: 1.0)
    static let vault_color_7: UIColor = UIColor(red: 0.0 / 255, green: 208.0 / 255, blue: 156.0 / 255, alpha: 1.0)
    static let vault_color_8: UIColor = UIColor(red: 55.0 / 255, green: 69.0 / 255, blue: 101.0 / 255, alpha: 1.0)
    static let vault_color_9: UIColor = UIColor(red: 41.0 / 255, green: 184.0 / 255, blue: 163.0 / 255, alpha: 1.0)
    static let vault_color_10: UIColor = UIColor(red: 73.0 / 255, green: 85.0 / 255, blue: 111.0 / 255, alpha: 1.0)
    static let vault_color_11: UIColor = UIColor(red: 39.0 / 255, green: 46.0 / 255, blue: 70.0 / 255, alpha: 1.0)
}
enum AppIcons {
    case abt
    case act
    case ada
    case adx
    case ae
    case agi
    case back
    case backspace
    case book
    case create_wallet
    case db
    case dot
    case dropdown
    case eth
    case eos
    case ven
    case bat
    case error
    case finger_print
    case flag
    case heart
    case import_wallet
    case left_menu
    case message_send
    case money
    case newwindow
    case next
    case omg
    case page1
    case page2
    case page3
    case page4
    case qrcode
    case sample_avatar
    case scan
    case search
    case small_qrcode
    case splash
    case star
    case sync
    case unnamed
    
    func getIcon() -> UIImage {
        switch self {
        case .abt:
            return SVGKImage(named: "abt").uiImage
        case .act:
            return SVGKImage(named: "act").uiImage
        case .ada:
            return SVGKImage(named: "ada").uiImage
        case .adx:
            return SVGKImage(named: "adx").uiImage
        case .ae:
            return SVGKImage(named: "ae").uiImage
        case .agi:
            return SVGKImage(named: "agi").uiImage
        case .back:
            return SVGKImage(named: "back").uiImage
        case .backspace:
            return SVGKImage(named: "backspace").uiImage
        case .book:
            return SVGKImage(named: "book").uiImage
        case .create_wallet:
            return SVGKImage(named: "create-wallet").uiImage
        case .db:
            return SVGKImage(named: "db").uiImage
        case .dot:
            return SVGKImage(named: "dot").uiImage
        case .dropdown:
            return SVGKImage(named: "dropdown").uiImage
        case .eth:
            return SVGKImage(named: "eth").uiImage
        case .eos:
            return SVGKImage(named: "eos").uiImage
        case .ven:
            return SVGKImage(named: "ven").uiImage
        case .bat:
            return SVGKImage(named: "bat").uiImage
        case .error:
            return SVGKImage(named: "error").uiImage
        case .finger_print:
            return SVGKImage(named: "finger-print").uiImage
        case .flag:
            return SVGKImage(named: "flag").uiImage
        case .heart:
            return SVGKImage(named: "heart").uiImage
        case .import_wallet:
            return SVGKImage(named: "import-wallet").uiImage
        case .left_menu:
            return SVGKImage(named: "left-menu").uiImage
        case .message_send:
            return SVGKImage(named: "message-send").uiImage
        case .money:
            return SVGKImage(named: "money").uiImage
        case .newwindow:
            return SVGKImage(named: "newwindow").uiImage
        case .next:
            return SVGKImage(named: "next").uiImage
        case .omg:
            return SVGKImage(named: "omg").uiImage
        case .page1:
            return SVGKImage(named: "page1").uiImage
        case .page2:
            return SVGKImage(named: "page2").uiImage
        case .page3:
            return SVGKImage(named: "page3").uiImage
        case .page4:
            return SVGKImage(named: "page4").uiImage
        case .qrcode:
            return SVGKImage(named: "qrcode").uiImage
        case .sample_avatar:
            return SVGKImage(named: "sample_avatar").uiImage
        case .scan:
            return SVGKImage(named: "scan").uiImage
        case .search:
            return SVGKImage(named: "search").uiImage
        case .small_qrcode:
            return SVGKImage(named: "small-qrcode").uiImage
        case .splash:
            return SVGKImage(named: "splash").uiImage
        case .star:
            return SVGKImage(named: "star").uiImage
        case .sync:
            return SVGKImage(named: "sync").uiImage
        case .unnamed:
            return SVGKImage(named: "unnamed").uiImage
        }
    }
    
    init?(strIcon: String) {
        switch (strIcon) {
        case "abt":
            self = .abt
        case "act":
            self = .act
        case "ada":
            self = .ada
        case "adx":
            self = .adx
        case "ae":
            self = .ae
        case "agi":
            self = .agi
        case "back":
            self = .back
        case "backspace":
            self = .backspace
        case "book":
            self = .book
        case "create-wallet":
            self = .create_wallet
        case "db":
            self = .db
        case "dot":
            self = .dot
        case "dropdown":
            self = .dropdown
        case "eth":
            self = .eth
        case "eos":
            self = .eos
        case "ven":
            self = .ven
        case "bat":
            self = .bat
        case "error":
            self = .error
        case "finger-print":
            self = .finger_print
        case "flag":
            self = .flag
        case "heart":
            self = .heart
        case "import-wallet":
            self = .import_wallet
        case "left-menu":
            self = .left_menu
        case "message-send":
            self = .message_send
        case "money":
            self = .money
        case "newwindow":
            self = .newwindow
        case "next":
            self = .next
        case "omg":
            self = .omg
        case "page1":
            self = .page1
        case "page2":
            self = .page2
        case "page3":
            self = .page3
        case "page4":
            self = .page4
        case "qrcode":
            self = .qrcode
        case "sample_avatar":
            self = .sample_avatar
        case "scan":
            self = .scan
        case "search":
            self = .search
        case "small-qrcode":
            self = .small_qrcode
        case "splash":
            self = .splash
        case "star":
            self = .star
        case "sync":
            self = .sync
        case "unnamed":
            self = .unnamed
        default:
            self = .back
        }
    }
    
}

